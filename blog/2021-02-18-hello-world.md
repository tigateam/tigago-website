---
slug: hello-world
title: It's very nice to meet you.
author: Misitebao
author_title: TigaTeam
author_url: https://github.com/misitebao
author_image_url: https://cdn.jsdelivr.net/gh/misitebao/CDN@master/gravatar.png
tags: [tigago]
---

It's very nice to meet you.
